from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeIngredient

# Register your models here.

# Registers 'Recipe' model
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

# Registers 'RecipeStep' model
@admin.register(RecipeStep)
class RecipeStep(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )

# Registers 'RecipeIngredient' model
@admin.register(RecipeIngredient)
class RecipeIngredient(admin.ModelAdmin):
    table_display = (
        "amount",
        "food_item",
        "id",
    )
