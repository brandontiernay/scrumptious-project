# Defining the Django model form class is a four-step process:

# 1) We define a Django model form class by writing our own class that inherits from the django.forms.ModelForm class.
# 2) Then, we create what's called an "inner class" named Meta, which is just a class inside a class.
# 3) Then, we specify which Django model it should work with.
# 4) Finally, we specify which fields we want to show.

# Following those steps, we'd end up with this code in the forms.py file:

from django import forms
from recipes.models import Recipe
from django.forms import ModelForm


class RecipeForm(ModelForm):    # Step 1
    email_address = forms.EmailField(max_length=300)

    class Meta:                 # Step 2
        model = Recipe          # Step 3
        fields = [              # Step 4
            "title",            # Step 4
            "picture",          # Step 4
            "description",      # Step 4
        ]                       # Step 4
