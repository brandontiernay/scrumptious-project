from django.db import models
from django.conf import settings

# Model to create recipes
class Recipe(models.Model):
    title = models.CharField(max_length = 200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

# ForeignKey back the User model
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, # Recommended way to refer to the User model
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True, # Existing recipes in database will not have an author assigned to them
    )

# The def __str__(self) method is what Python will use when it wants a string
# representation of the recipe object.
    def __str__(self):
        return self.title


# Model to create the steps to make each recipe
class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()

# ForeignKey back to the 'Recipe' model
    recipe = models.ForeignKey(
        'Recipe',
        related_name="steps",
        on_delete=models.CASCADE,
    )
# Has it default the ordering to step_number
    class Meta:
        ordering = ["step_number"]


# Model to create the ingredients of each recipe
class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)

# ForeignKey back to the 'Recipe' model
    recipe = models.ForeignKey(
        'Recipe',
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
# Has it default the ordering to food_item
    class Meta:
        ordering = ["food_item"]
